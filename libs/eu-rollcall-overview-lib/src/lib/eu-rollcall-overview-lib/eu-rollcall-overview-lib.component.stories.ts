import type { Meta, StoryObj } from "@storybook/angular";
import { EuRollcallOverviewLibComponent } from "./eu-rollcall-overview-lib.component";

import { within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

const meta: Meta<EuRollcallOverviewLibComponent> = {
    component: EuRollcallOverviewLibComponent,
    title: "EuRollcallOverviewLibComponent",
};
export default meta;
type Story = StoryObj<EuRollcallOverviewLibComponent>;

export const Primary: Story = {
    args: {},
};

export const Heading: Story = {
    args: {},
    play: async ({ canvasElement }) => {
        const canvas = within(canvasElement);
        expect(
            canvas.getByText(/eu-rollcall-overview-lib works!/gi)
        ).toBeTruthy();
    },
};
