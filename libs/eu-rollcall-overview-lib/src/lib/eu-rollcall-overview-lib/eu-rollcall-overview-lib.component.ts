import { Component } from "@angular/core";
import { CommonModule } from "@angular/common";

@Component({
    selector: "lib-eu-rollcall-overview-lib",
    standalone: true,
    imports: [CommonModule],
    templateUrl: "./eu-rollcall-overview-lib.component.html",
    styleUrl: "./eu-rollcall-overview-lib.component.css",
})
export class EuRollcallOverviewLibComponent {}
