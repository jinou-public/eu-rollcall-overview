import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EuRollcallOverviewLibComponent } from "./eu-rollcall-overview-lib.component";

describe("EuRollcallOverviewLibComponent", () => {
    let component: EuRollcallOverviewLibComponent;
    let fixture: ComponentFixture<EuRollcallOverviewLibComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [EuRollcallOverviewLibComponent],
        }).compileComponents();

        fixture = TestBed.createComponent(EuRollcallOverviewLibComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
