import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NxWelcomeComponent } from './nx-welcome.component';
import {EuroParlService} from "./EuroParlService";

@Component({
  standalone: true,
  imports: [NxWelcomeComponent, RouterModule, EuroParlService],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'angular-monorepo';

  constructor(private euroParlService: EuroParlService) {
    this.euroParlService.getMeps({}).subscribe((meps) => {
      console.log(meps);
    });
  }

}
