import {Injectable} from '@angular/core';
import {
  Configuration,
  ControlledVocabulariesGetRequest,
  CorporateBodiesGetRequest,
  DefaultApi,
  DocumentsGetRequest,
  MeetingsGetRequest,
  MepsGetRequest
} from "@jinou-public/eu-rollcall-overview-fetch";


import {from, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EuroParlService {
  private api: DefaultApi = new DefaultApi(new Configuration(
    { basePath: 'http://localhost:8080' }
  ));

  // MEPs
  getMeps(request: MepsGetRequest): Observable<string> {
    return from(this.api.mepsGet(request));
  }

  getMepById(mepId: number): Observable<string> {
    return from(this.api.mepsMepIdGet({ mepId }));
  }

  // Corporate Bodies
  getCorporateBodies(request: CorporateBodiesGetRequest): Observable<string> {
    return from(this.api.corporateBodiesGet(request));
  }

  getCorporateBodyById(bodyId: string): Observable<string> {
    return from(this.api.corporateBodiesBodyIdGet({ bodyId }));
  }

  // Meetings
  getMeetings(request: MeetingsGetRequest): Observable<string> {
    return from(this.api.meetingsGet(request));
  }

  getMeetingById(eventId: string): Observable<string> {
    return from(this.api.meetingsEventIdGet({ eventId }));
  }

  // Documents
  getDocuments(request: DocumentsGetRequest): Observable<string> {
    return from(this.api.documentsGet(request));
  }

  getDocumentById(docId: string): Observable<string> {
    return from(this.api.documentsDocIdGet({ docId }));
  }

  // Controlled Vocabularies
  getControlledVocabularies(request: ControlledVocabulariesGetRequest): Observable<string> {
    return from(this.api.controlledVocabulariesGet(request));
  }

  getControlledVocabularyById(vocId: string): Observable<string> {
    return from(this.api.controlledVocabulariesVocIdGet({ vocId }));
  }
}
