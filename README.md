# JiventAdminuiFrontend

## Application

**Start the application:**
```shell
npx nx serve eu-rollcall-overview
```

**Running unit tests:**
```shell
nx test eu-rollcall-overview-lib
```

**Build for production:**
```shell
npx nx build eu-rollcall-overview
```
The build artifacts are stored in the output directory (e.g. `dist/` or `build/`), ready to be deployed.

## Lib
**Start Storybook:**
```shell
nx run eu-rollcall-overview-lib:storybook
```

**Running unit tests:**
```shell
nx test eu-rollcall-overview-lib
```

**Build for development:**
```shell
npx nx build eu-rollcall-overview-lib --watch
```

**Build for production:**
```shell
npx nx build eu-rollcall-overview-lib
```


## Running tasks

To execute tasks with Nx use the following syntax:

```
npx nx <target> <project> <...options>
```

You can also run multiple targets:

```
npx nx run-many -t <target1> <target2>
```

..or add `-p` to filter specific projects

```
npx nx run-many -t <target1> <target2> -p <proj1> <proj2>
```

Targets can be defined in the `package.json` or `projects.json`. Learn more [in the docs](https://nx.dev/features/run-tasks).
